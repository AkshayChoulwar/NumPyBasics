import numpy as np

#creating numpy ndarray object

data_list=[[1,1,1],[2,2,2],[3,3,3]]

ndarray_object=np.array(data_list)
#printing dimensions and type of ndarray object
print ndarray_object.shape,ndarray_object.dtype

#creating ndarray of zeroes with size 10
ndarray_zeroes=np.zeros(10)
ndarray_zeroes

#creating ndarray of zeroes of dimension 2
nd_z=np.zeros((2,2))
nd_z

#creating ndarray of particular range
ndarray_range=np.arange(8)

#indexing
one=ndarray_range[2:5]
one

#assigning values to slices the changes reflected to original array
ndarray_range[2:5]=111
one[:]=111
ndarray_range

#indexing in two dimensional array
ndarray_object[2:]
#slicing horizontally as well as vertically
ndarray_object[:2,:2]

#transposing array and reshaping it
mat=np.arange(15).reshape((3,5))
k1=mat.T
k2=np.random.randn(3,5)
k2

#dot product of two matrises
np.dot(k2.T,k2)